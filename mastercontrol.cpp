#include <Dry/Graphics/Geometry.h>
#include <Dry/Graphics/GraphicsEvents.h>
#include <Dry/Graphics/IndexBuffer.h>
#include <Dry/Graphics/VertexBuffer.h>

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl* MasterControl::instance_ = NULL;

MasterControl* MasterControl::GetInstance()
{
    return MasterControl::instance_;
}

MasterControl::MasterControl(Context *context): Application(context),
    randomInterval_{},
    untilRandom_{ 0.0f },
    animate_{ false }
{
    instance_ = this;
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());
    randomInterval_ = Random(M_MIN_NEARCLIP, M_PHI);

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "airwitch.log";
    engineParameters_[EP_WINDOW_TITLE] = "AirWitch";
    engineParameters_[EP_WINDOW_RESIZABLE] = true;
//    engineParameters_[EP_WINDOW_ICON] = "";
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_FULL_SCREEN] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Resources";
}
void MasterControl::Start()
{
    if (GRAPHICS)
        ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());

    CreateScene();

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    UI* ui{ GetSubsystem<UI>() };
    Input* input{ GetSubsystem<Input>() };
    input->SetMouseMode(MM_FREE);
    input->SetMouseVisible(true);

    // Construct new Text object, set string to display and font to use
    Text* instructionText{ ui->GetRoot()->CreateChild<Text>() };
    instructionText->SetText("Scroll to change transition speed\nMiddle mouse button to pause");
    instructionText->SetFont(cache->GetResource<Font>("Fonts/Philosopher.ttf"), 16);
    instructionText->SetColor(Color::GRAY * 0.75f);
    instructionText->SetEffectColor(Color::BLUE * 0.5f);
    instructionText->SetTextEffect(TE_SHADOW);
    instructionText->SetTextAlignment(HA_LEFT);

    // Position the text relative to the screen
    instructionText->SetHorizontalAlignment(HA_LEFT);
    instructionText->SetVerticalAlignment(VA_TOP);
    instructionText->SetPosition(ui->GetRoot()->GetHeight() / 64, ui->GetRoot()->GetHeight() / 64);

    SubscribeToEvent(E_UPDATE, DRY_HANDLER(MasterControl, HandleUpdate));
    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(MasterControl, HandleScreenMode));
}
void MasterControl::Stop()
{
    engine_->DumpResources(true);
}
void MasterControl::Exit()
{
    engine_->Exit();
}
void MasterControl::HandleScreenMode(StringHash eventType, VariantMap& eventData)
{
    IntVector2 screenSize{ GRAPHICS->GetSize() };

    for (int v{ 0 }; v < RENDERER->GetNumViewports(); ++v)
    {
        Viewport* port{ RENDERER->GetViewport(v) };
        int portSize{ screenSize.y_ / 3 };

        if (v == 0)
            port->SetRect({ 0, 0, screenSize.x_ - portSize, screenSize.y_ });
        else
        {
            IntVector2 portPos{ screenSize.x_ - portSize, portSize * (v - 1) };
            port->SetRect({ portPos.x_, portPos.y_, portPos.x_ + portSize,  portPos.y_ + portSize });
        }
    }

}

void MasterControl::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    bool turnTable{ false };
    Input* input{ GetSubsystem<Input>() };
    float t{ eventData[Update::P_TIMESTEP].GetFloat() };

    if (input->GetMouseButtonPress(MOUSEB_MIDDLE))
        animate_ ^= true;

    randomInterval_ -= Sqrt(randomInterval_) * input->GetMouseMoveWheel() * 0.1f;
    if (randomInterval_ < M_LARGE_EPSILON)
        randomInterval_ = M_LARGE_EPSILON;

    if (animate_)
        untilRandom_ -= t;
    if (untilRandom_ <= 0.0f)
    {
        untilRandom_ = randomInterval_;
        wand_= targetWand_;
        targetWand_ = Wand{ { targetWand_.anode().Position(), randomNormal() },
                            { targetWand_.cathode().Position(), randomNormal() } };
    }

    if (input->GetKeyDown(KEY_ESCAPE) && input->GetMouseMode() == MM_WRAP)
    {
        input->SetMouseMode(MM_FREE); input->SetMouseVisible(true);
    }
    else if (input->GetMouseButtonPress(MOUSEB_ANY))
    {
        input->SetMouseMode(MM_WRAP); input->SetMouseVisible(false);
    }

    Node* camNode{ scene_->GetChild("Camera") };
    if (!turnTable && input->GetMouseMode() == MM_WRAP)
    {
        Quaternion spin{ 0.0f, 17.0f * input->GetMouseMoveX() * t, 0.0f };
        camNode->RotateAround(Vector3::ZERO, spin, TS_WORLD);
        scene_->GetChild("Light")->RotateAround(Vector3::ZERO,  spin, TS_WORLD);

        spin = Quaternion{ 17.0f * input->GetMouseMoveY() * t, camNode->GetRight() };
        camNode->RotateAround(Vector3::ZERO, spin, TS_WORLD);
    }
    else
    {
        Quaternion spin{ 0.0f * t, 7.0f * t, 0.0f * t };
        scene_->GetChild("Camera")->RotateAround(Vector3::ZERO, spin, TS_WORLD);
        scene_->GetChild("Light")->RotateAround(Vector3::ZERO,  spin, TS_WORLD);
    }

    Vector2 leftStick{};
    Vector2 rightStick{};
    bool circle{ false };

    if (input->GetNumJoysticks() > 0)
    {
        JoystickState* jss{ input->GetJoystick(0) };
        leftStick.x_ = jss->GetAxisPosition(0);
        leftStick.y_ = jss->GetAxisPosition(1);
        rightStick.x_ = jss->GetAxisPosition(2);
        rightStick.y_ = jss->GetAxisPosition(3);
        circle = jss->GetButtonDown(CONTROLLER_BUTTON_B);
    }

    Wand wand{};
    if (animate_)
    {
        wand = Wand{
            { wand_.anode().Position(),   slerpNormal(wand_.anode().Normal(), targetWand_.anode().Normal(), 0.5f + 0.5f * cos( M_PI * (untilRandom_ / randomInterval_))) },
            { wand_.cathode().Position(), slerpNormal(wand_.cathode().Normal(), targetWand_.cathode().Normal(), 0.5f + 0.5f * cos( M_PI * (untilRandom_ / randomInterval_))) } };
    }
    else if (leftStick.Length() > M_MIN_NEARCLIP || rightStick.Length() > M_MIN_NEARCLIP)
    {
        wand = Wand{
        { wand_.anode().Position(),   Quaternion{ 0.23f * leftStick.y_, wand_.direction() } * Quaternion{ -0.23f * leftStick.x_, camNode->GetDirection() } * wand_.anode().Normal() },
        { wand_.cathode().Position(), Quaternion{ 0.23f * rightStick.y_, wand_.direction() } * Quaternion{ -0.23f * rightStick.x_, camNode->GetDirection() } * wand_.cathode().Normal() } };

        wand_ = wand;
    }
    else if (circle)
    {
        wand = Wand{
        { wand_.anode().Position(),     wand_.anode().Normal().ProjectOntoPlane(wand_.left()) },
        { wand_.cathode().Position(), wand_.cathode().Normal().ProjectOntoPlane(wand_.left()) } };
        wand_ = wand;
    }

    if (wand.broom().Length())
    {
        Vector<Rune> runes{ wand.Plot(32u, Vector3::LEFT) };

        witchNode_ = scene_->GetChild("Witch");
        StaticModel* curve{ witchNode_->GetComponent<StaticModel>() };
        curve->SetModel(Line(runes));
        curve->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Gloo.xml"));

        CreateTickmarks({ runes });
    }
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<PhysicsWorld>();
    scene_->CreateComponent<DebugRenderer>();

    //Light
    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition(Vector3(5.0f, 23.0f, 42.0f));
    lightNode->LookAt(Vector3::ZERO);
    Light* light{ lightNode->CreateComponent<Light>() };
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetRange(420.0f);
    //Camera
    Node* cameraNode{ scene_->CreateChild("Camera") };
    cameraNode->SetPosition(Vector3{ 5.0f, 5.0f, -64.0f } * 4);
    cameraNode->LookAt(Vector3::ZERO);
    Camera* camera{ cameraNode->CreateComponent<Camera>() };
    camera->SetFov(23);

    IntVector2 screenSize{ GRAPHICS->GetSize() };
    int portSize{ GRAPHICS->GetSize().y_ / 3 };
    RENDERER->SetViewport(0, new Viewport(context_, scene_, camera, { 0, 0, screenSize.x_ - portSize, screenSize.y_ }));

    const float distance{55.0f};
    for (int c{ 0 }; c < 3; ++c)
    {
        Vector3 camPos{ distance * Vector3::BACK };
        IntVector2 portPos{ screenSize.x_ - portSize, portSize * c };
        if (c < 2)
            camPos = Quaternion{ 90.0f, (c == 1 ? Vector3::RIGHT : Vector3::UP) } * camPos;
        Node* portCamNode{ scene_->CreateChild("PortCamera") };
        portCamNode->SetPosition(camPos);
        portCamNode->LookAt(Vector3::ZERO);
        Camera* portCam{ portCamNode->CreateComponent<Camera>() };
        portCam->SetOrthographic(true);
        portCam->SetOrthoSize(100.0f);
        IntRect portRect{ portPos.x_, portPos.y_, portPos.x_ + portSize,  portPos.y_ + portSize };
        Viewport* port( new Viewport(context_, scene_, portCam) );

        port->SetRect(portRect);
        RENDERER->SetViewport(c + 1, port);
    }

    const float r{ 42.0f };
    const Vector3 start{ Vector3::LEFT * r };
    const Vector3 end{ Vector3::RIGHT * r };
    const Vector3 n0{ -0.5f, 0.5f, 0.0f };
    const Vector3 n1{  0.5f, 0.5f, 0.0f };

    targetWand_ = Wand{ { start, randomNormal() }, { end, randomNormal() } };
    wand_ = Wand{ { start, n0 }, { end, n1 } };
    Vector<Rune> runes{ wand_.Plot(128u, Vector3::UP) };

    witchNode_ = scene_->CreateChild("Witch");
    StaticModel* curve{ witchNode_->CreateComponent<StaticModel>() };
    curve->SetModel(Line(runes));
    curve->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Gloo.xml"));

    CreateTickmarks({ runes });
}

SharedPtr<Model> MasterControl::Line(const Vector<Rune>& runes)
{
    const unsigned numVertices{ runes.Size() };

    PODVector<float> vertexData{};
    PODVector<unsigned short> indexData{};

    int i{ 0 };
    for (const Rune& r: runes)
    {
        vertexData.Push(r.Position().x_);
        vertexData.Push(r.Position().y_);
        vertexData.Push(r.Position().z_);
        vertexData.Push(r.Normal().x_);
        vertexData.Push(r.Normal().y_);
        vertexData.Push(r.Normal().z_);

        indexData.Push(i++);
    }

    SharedPtr<Model> fromScratchModel{ new Model{ context_ } };
    SharedPtr<VertexBuffer> vb{ new VertexBuffer{ context_ } };
    SharedPtr<IndexBuffer> ib{ new IndexBuffer{ context_ } };
    SharedPtr<Geometry> geom{ new Geometry{ context_ } };

    // Shadowed buffer needed for raycasts to work, and so that data can be automatically restored on device loss
    vb->SetShadowed(true);
    // We could use the "legacy" element bitmask to define elements for more compact code, but let's demonstrate
    // defining the vertex elements explicitly to allow any element types and order
    PODVector<VertexElement> elements;
    elements.Push(VertexElement{ TYPE_VECTOR3, SEM_POSITION });
    elements.Push(VertexElement{ TYPE_VECTOR3, SEM_NORMAL });
    vb->SetSize(numVertices, elements);
    vb->SetData(vertexData.Buffer());

    ib->SetShadowed(true);
    ib->SetSize(numVertices, false);
    ib->SetData(indexData.Buffer());

    geom->SetVertexBuffer(0, vb);
    geom->SetIndexBuffer(ib);
    geom->SetDrawRange(LINE_STRIP, 0, numVertices);

    fromScratchModel->SetNumGeometries(1);
    fromScratchModel->SetGeometry(0, 0, geom);
    fromScratchModel->SetBoundingBox(BoundingBox{ -0.5f, 0.5f });

    // Though not necessary to render, the vertex & index buffers must be listed in the model so that it can be saved properly
    Vector<SharedPtr<VertexBuffer> > vertexBuffers;
    Vector<SharedPtr<IndexBuffer> > indexBuffers;
    vertexBuffers.Push(vb);
    indexBuffers.Push(ib);
    // Morph ranges could also be not defined. Here we simply define a zero range (no morphing) for the vertex buffer
    PODVector<unsigned> morphRangeStarts;
    PODVector<unsigned> morphRangeCounts;
    morphRangeStarts.Push(0);
    morphRangeCounts.Push(0);
    fromScratchModel->SetVertexBuffers(vertexBuffers, morphRangeStarts, morphRangeCounts);
    fromScratchModel->SetIndexBuffers(indexBuffers);

    return fromScratchModel;
}

void MasterControl::CreateTickmarks(const Vector<Rune>& runes)
{
    if (tickNode_)
        tickNode_->Remove();

    tickNode_ = witchNode_->CreateChild("Ticks");

    const Vector3& start{ runes.Front().Position() };
    const Vector3& end{ runes.Back().Position() };
    const Vector3 broom{ end - start };
    float r{ broom.Length() * 0.5f };
    const float tickSize{ 0.1f * r };
    const float numTicks{ 010 };
    const float tickAngle{ 360.0f / numTicks };

    for (int t{ 0 }; t < numTicks; ++t)
    {
        Vector3 tickStart{ 0.0f, Cos(t * tickAngle) * r, Sin(t * tickAngle) * r };
        Vector3 left{ tickStart.CrossProduct(end - start).Normalized() };
        Vector<Rune> tick{ Vector3{ tickStart },
                           Vector3{ tickStart + tickSize * (tickStart.Normalized() + left * 0.23f) },
                           Vector3{ tickStart + tickSize * (tickStart.Normalized() * 0.8f) },
                           Vector3{ tickStart + tickSize * (tickStart.Normalized() - left * 0.23f) },
                           Vector3{ tickStart } };

        StaticModel* sm{ tickNode_->CreateComponent<StaticModel>() };
        sm->SetModel(Line(tick));
        sm->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Goo.xml"));
    }

    for (const Rune& r: runes)
    {
        Vector3 tickStart{ r.Position() };
        Vector<Rune> tick{ Vector3{ tickStart }, Vector3{ tickStart + (2.3f - 1.7f * (tickStart != start && tickStart != end)) * tickSize * r.Normal() }};

        StaticModel* sm{ tickNode_->CreateChild()->CreateComponent<StaticModel>() };
        sm->SetModel(Line(tick));
        sm->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Goo.xml"));
    }
    StaticModel* midline{ tickNode_->CreateComponent<StaticModel>() };
    midline->SetModel(Line({{ start - broom }, { end + broom }}));
    midline->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Groo.xml"));

    StaticModel* circle{ tickNode_->CreateComponent<StaticModel>() };
    Wand arcWand0{ { start, -broom +0.01f*Vector3::UP }, { end, broom } };///UGLY
    Wand arcWand1{ { end, broom-0.01f*Vector3::UP }, { start, -broom  } };///UGLY
    Vector<Rune> arc0{ arcWand0.Plot(31u, Vector3::UP) };
    arc0.Push(arcWand1.Plot(31u, Vector3::DOWN));

    circle->SetModel(Line(arc0));
    circle->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Groo.xml"));
}
