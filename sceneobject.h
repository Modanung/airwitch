
#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include <Dry/Dry.h>

#include "mastercontrol.h"

class SceneObject : public LogicComponent
{
    DRY_OBJECT(SceneObject, Object);
public:
    SceneObject(Context* context);

    virtual void Set(Vector3 position);
    void OnNodeSet(Node *node) override;

    Vector3 GetWorldPosition() const;
protected:
    unsigned initialSoundSources_;
    Vector<SoundSource3D*> sampleSources_;
    float randomizer_;

    void Disable();

    void PlaySample(Sound *sample, float gain = 0.3f);
private:
    void AddSoundSource();
};

#endif // SCENEOBJECT_H

