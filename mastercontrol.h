
#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include <Dry/Dry.h>

#include "luckey.h"

namespace Dry {
class Node;
class Scene;
}

struct Rune: public Pair<Vector3, Vector3>
{
    using Pair::Pair;

    Rune(const Vector3& position = Vector3::ZERO, const Vector3& normal = Vector3::UP): Pair<Vector3, Vector3>(position, normal.Normalized()) {}
    Vector3 Position() const { return first_; }
    Vector3 Normal() const { return second_; }
};

struct Wand: Pair<Rune, Rune>
{
    using Pair::Pair;

    Vector<Rune> Plot(unsigned resolution, const Vector3& edgeNormal = Vector3::ZERO)
    {
        if (resolution == 0)
            return { first_, second_ };

        Vector<Rune> curve{};
        unsigned numVerts{ resolution + 2 };

        for (unsigned i{ 0 }; i < numVerts; ++i)
        {
            float t{ i * 1.0f / (numVerts - 1) };
            curve.Push(Plot(t, edgeNormal));
        }

        return curve;
    }

    const Rune& anode() const { return first_ ; }
    const Rune& cathode() const { return second_ ; }
    Vector3 broom() const { return cathode().Position() - anode().Position(); }
    Vector3 direction() const { return broom().Normalized(); }
    Vector3 up(const Vector3& edgeNormal = Vector3::ZERO) const
    {
        Vector3 adn{ anode()  .Normal().ProjectOntoPlane(direction()).Normalized() };
        Vector3 cdn{ cathode().Normal().ProjectOntoPlane(direction()).Normalized() };
        Quaternion rot{ adn, cdn };

        Vector3 normal{ (Quaternion::IDENTITY.Slerp(rot, 0.5f) * anode().Normal()).ProjectOntoPlane(direction()).Normalized() };
        if (edgeNormal.Length() != 0.0f)
        {
            if (normal.Length() == 0.0f)
                normal = edgeNormal.ProjectOntoPlane(direction()).Normalized();
            if (normal.DotProduct(edgeNormal) < 0.0f)
                normal = -normal;
        }

        return normal;
    }
    Vector3 left(const Vector3& edgeNormal = Vector3::ZERO) const
    {
        Quaternion rot{};
        rot.FromAngleAxis(-90, direction());
        return rot * -up(edgeNormal);
    }


    Rune Plot(float t, const Vector3& edgeNormal = Vector3::ZERO)
    {
        if (t <= 0.0f)
            return anode();
        else if (t >= 1.0f)
            return  cathode();

        PODVector<Vector3> segment{};
        for (int i{ 0 }; i < 3; ++i)
        {
            float dt{ (i == 0 ? 0.0f : M_LARGE_EPSILON * (2 * (i - 1) - 1 )) };
            segment.Push(PlotPos(t + dt, edgeNormal));
        }

        Vector3 d{ (segment[2] - segment[1]).Normalized() };
        Vector3 vIn{ anode().Normal().ProjectOntoPlane(direction()).Normalized() };
        Vector3 vOut{ cathode().Normal().ProjectOntoPlane(direction()).Normalized() };

        if (vIn.LengthSquared() == 0.0f)
            vIn = edgeNormal.ProjectOntoPlane(direction()).Normalized();
        if (vOut.LengthSquared() == 0.0f)
            vOut = edgeNormal.ProjectOntoPlane(direction()).Normalized();

        Wand normWand{ { vIn, vIn }, { vOut, vOut }};
        Vector3 v{ normWand.PlotPos(t, edgeNormal).Normalized() };
        float slope{ v.DotProduct(d) };

        Vector3 normal{ v * cos(M_PI_2 * slope) - slope * direction() * sin(M_PI * Abs(slope)) };

        float at{ pow(Min(1.0f, Max(0.0f, 1.0f - t * 2.0f)), 4.0f) };
        float ct{ pow(Min(1.0f, Max(0.0f, t * 2.0f - 1.0f)), 4.0f) };
        normal = normal.Lerp(anode().Normal(), at).Lerp(cathode().Normal(), ct);

        return { segment[0], normal.Normalized() };
    }

    Vector3 PlotPos(float t, const Vector3& edgeNormal = Vector3::ZERO)
    {
        t = Clamp(t, 0.0f, 1.0f);
        float sinPiT{ sin(M_PI * t) };
        float cosPiT{ cos(M_PI * t) };

        float in{ -direction().DotProduct(anode()  .Normal()) };
        float out{ direction().DotProduct(cathode().Normal()) };
        float th{ Lerp(in, out, t) };

        Vector3 vIn{ anode().Normal().ProjectOntoPlane(direction()).Normalized() };
        Vector3 vOut{ cathode().Normal().ProjectOntoPlane(direction()).Normalized() };

        if (vIn.LengthSquared() == 0.0f)
            vIn = edgeNormal;
        if (vOut.LengthSquared() == 0.0f)
            vOut = edgeNormal;

        Vector3 v{ Quaternion::IDENTITY.Slerp({ vIn, vOut }, t) * vIn };
        Vector3 lateral{ Lerp(Vector3::ZERO, v * sinPiT / 2, Abs(th)) };
        if (th < 0.0f)
            lateral = -lateral;
        float z{ Lerp(t, (1 - cosPiT) / 2, th * th) };
        float m{ broom().Length() };

        float att{ cbrt(atan(th * th * M_PI_2)) };
        Vector3 delta{ m * (att * lateral + direction() * z) };

        return anode().Position() + delta;
    }
};


class MasterControl : public Application
{
    DRY_OBJECT(MasterControl, Application);
public:
    MasterControl(Context* context);
    static MasterControl* GetInstance();

    Scene* GetScene() const { return scene_; }


    // Setup before engine initialization. Modifies the engine paramaters.
    virtual void Setup();
    // Setup after engine initialization.
    virtual void Start();
    // Cleanup after the main loop. Called by Application.
    virtual void Stop();
    void Exit();

    template <class T> void RegisterObject() { context_->RegisterFactory<T>(); }
    template <class T> void RegisterSubsystem() { context_->RegisterSubsystem(new T(context_)); }
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
private:
    void CreateScene();

    static MasterControl* instance_;
    Scene* scene_;
    Node* tickNode_;
    Node* witchNode_;
    Wand wand_;
    Wand targetWand_;
    float randomInterval_;
    float untilRandom_;
    bool animate_;
    SharedPtr<Model> Line(const Vector<Rune>& runes);
    void CreateTickmarks(const Vector<Rune>& runes);
    void HandleScreenMode(StringHash eventType, VariantMap& eventData);
};

static Vector3 randomNormal() { return Vector3{ Random(-1.0f, 1.0f), Random(-1.0f, 1.0f), Random(-1.0f, 1.0f) }.Normalized(); }
static Vector3 slerpNormal(const Vector3& a, const Vector3& b, float t)
{
    Quaternion rot{ Quaternion::IDENTITY };
    rot = rot.Slerp(Quaternion{ a, b }, t*t );
    return rot * a;
}

#endif // MASTERCONTROL_H
